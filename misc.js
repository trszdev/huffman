//node.js

module.exports = {
    'BitWriter' : BitWriter,
    'BitReader' : BitReader,
    'GreatArray' : GreatArray,
    'repeat': repeat,
}

function BitReader(buffer){
    var self = this;
    var cbyte = 0;
    var cbit = 0;
    self.available_bits = () => self.available_bytes()*8 - cbit;
    self.available_bytes = () => buffer.length-cbyte;
    self.read_bit = function(){
        var bit = !!(buffer[cbyte] & (1<<(7-cbit)))-0;
        if (cbit++==7){
            cbit = 0;
            cbyte++;
        }
        return bit;
    }
    self.read_byte = function(){
        var num = 0;
        for(var i=0;i<8;i++)
            num |= self.read_bit()<<(7-i);
        return num;
    }
    self.get_modulus = ()=>(8-cbit)%8;
    Object.freeze(self);
    return self;
}


function BitWriter(){
    var self = this;
    var cbit = 0;
    var cbyte = 0;
    var bytes = [];
    self.get_modulus = ()=>(8-cbit)%8;
    self.write_bit = function(bit){
        cbyte |= (bit%2)<<(7-cbit);
        if (cbit++==7){
            bytes.push(cbyte);
            cbit = cbyte = 0;
        }
    }
    self.write_byte = function(num){
        for(var i=7;i>=0;i--)
            self.write_bit(!!(num & (1<<i))-0);
    }
    self.get_bytes = ()=>{
        if (cbit!=0) 
            return bytes.concat(cbyte);
        return bytes.map(a=>a);
    };
    Object.freeze(self);
    return self;
}


function GreatArray() {
    list = arguments[0] || [];
    list.extend = function(seq){
        Array.prototype.push.apply(list, seq);
    }
    list.traverse = function(callback){
        while(list.length!=0) {
            var elem = list.pop();
            callback(elem);
        }
    }
    list.indexOf = function(e){
        if (typeof(e)=='function'){
            for(var i=0;i<list.length;i++)
                if (e(list[i])) return i;
        }
        else {
            for(var i=0;i<list.length;i++)
                if (list[i]==e) return i; 
        }
        return -1;
    }
    return list;
}

function repeat(element, count) {
    return Array(count+1).join().split('')
        .map(a=>element);
}
