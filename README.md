# classic 8bit huffman coder


## exports:
 
1. encode(buffer) -> encode buffer as tree + bit sequence
2. decode(buffer[, params]) -> decode encoded buffer into initial  
   *__params[is_significant]=true__ throws errors if file is damaged someway*
3. run(args) -> run script as cmdtool inside custom scenario

*warning: can't compress some files due to tree storage*

## tests:


```
#!javascript

node test.js
```


## links:

* http://huffman.ooz.ie/
* http://stackoverflow.com/questions/759707/efficient-way-of-storing-huffman-tree#759707
* https://habrahabr.ru/post/144200/