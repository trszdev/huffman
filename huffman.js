// node.js
misc = require('./misc');

function Htree(htree){
    var self = this;
    self.lookup = [];
    self.rlookup = {};
    self.htree = htree;

    () => {
        var init = [!htree[2] && !htree[3] ? [0] : []]; 
        var nodes = misc.GreatArray([htree.concat(init)]);
        nodes.traverse((node) => {
            var path = node[4];
            if (node[0]==-1) {
                node[2][4] = path.concat([1]);
                node[3][4] = path.concat([0]);
                nodes.push(node[2]);
                nodes.push(node[3]);
            }
            else {
                self.lookup[node[0]] = node[4];
                self.rlookup[node[4]] = node[0];
            }
        });
        self.alphabet = Object.keys(self.rlookup)
            .map(a=>self.rlookup[a]);
    }();
    

    self.encode = function(){
        var bw = new misc.BitWriter();
        var nodes = misc.GreatArray([self.htree]);
        nodes.pop = nodes.shift; // hax
        nodes.traverse((node)=>{
            if (node[0]==-1) {
                bw.write_bit(0);
                nodes.push(node[2]);
                nodes.push(node[3]);
            }
            else {
                bw.write_bit(1);
                bw.write_byte(node[0]);
            }
        });
        return bw;
    }
}


Htree.from_encoded = function(bitReader){
    var br = bitReader;
    if (br.read_bit() == 1)
        return new Htree([br.read_byte(), 0, 0, 0]);
    var root = [-1, 0, 0, 0];
    var nodes = misc.GreatArray([root]);
    var counter = 0, unclosed = -2;
    nodes.pop = nodes.shift; // hax
    nodes.traverse((node) => {
        if (br.available_bits()<0) throw 'Tree is corrupted';
        node[2] = [-1, 0, 0, 0];
        if (br.read_bit() == 0) {
            unclosed--;
            nodes.push(node[2]); 
        } else node[2] = [br.read_byte(), (++unclosed, ++counter), 0, 0]; 
        node[3] = [-1, 0, 0, 0];
        if (br.read_bit() == 0) {
            unclosed--;
            nodes.push(node[3]);
        }
        else node[3] = [br.read_byte(), (++unclosed, ++counter), 0, 0]; 
    });
    if (counter>256 || br.available_bits()<0 || unclosed!=0)
        throw 'Tree is corrupted';
    return new Htree(root);
}


Htree.from_raw = function(buffer){
    var freq = misc.repeat(0, 256);
    var nodes = [];
    for(var i=0;i<buffer.length;i++)
        freq[buffer[i]]++;
    for(var i=0;i<256;i++) if (freq[i])
        nodes.push([i, freq[i], 0, 0]);
    while(nodes.length!=1) {
        nodes.sort((a,b)=>a[1] - b[1]);
        var a = nodes.shift(), b = nodes.shift();
        nodes.push([-1, a[1]+b[1], a, b]);
    }
    return new Htree(nodes[0]);
}


function encode(buffer) {
    if (buffer.length==0) return Buffer();
    var htree = Htree.from_raw(buffer);
    var lookup = htree.lookup;
    var bw = htree.encode();
    var br = new misc.BitReader(buffer);
    while(br.available_bytes()){
        var bits = lookup[br.read_byte()];
        bits.forEach(x=>bw.write_bit(x));
    }
    var mod = bw.get_modulus();
    var bytes = bw.get_bytes();
    return Buffer([mod].concat(bytes));
}


function decode(buffer, params){
    if (buffer.length==0) return Buffer();
    params = params || {};
    var read_param = (name, auto) => name in params ? params[name] : auto;
    var is_significant = !!read_param('is_significant', true);
    var br = new misc.BitReader(buffer);
    var mod = br.read_byte();
    if (buffer.length<3 || mod>=8) 
        throw 'Invalid file format';
    var htree = Htree.from_encoded(br);
    var root = htree.htree;
    if (htree.alphabet.length == 1) root[3] = root;
    var last_node = root;
    var result = [];
    while(br.available_bits()>mod){
        last_node = br.read_bit()==0 ? last_node[3] : last_node[2];
        if (last_node==0){
            if (is_significant) throw 'File is damaged';
            last_node = root;
            continue;
        }
        if (last_node[0] != -1){
            result.push(last_node[0]);
            last_node = root;
        }
    }
    if (is_significant && last_node!==root) throw 'File is damaged';
    return Buffer(result);
}


function main(args) {
    const fs = require('fs');
    var opt = ['encode', 'decode'].indexOf(args[2])
    if (args.length != 5 || opt==-1) {
        console.log('\tusage: node huffman.js [encode|decode] [infile] [outfile]');
        return 1;
    }
    var buffer = fs.readFileSync(args[3]);
    var result = !opt ? encode(buffer) : decode(buffer, {'is_significant' : false});
    fs.writeFileSync(args[4], result);
    return 0;
}


module.exports = {
    'Htree': Htree,
    'encode' : encode,
    'decode' : decode,
    'run' : main
}


if (require.main === module)
    process.exit(main(process.argv));
