huffman = require('./huffman');
fs = require('fs');
entropy = require('./entropy.js').count_entropy;

function msg(message){
    console.log('\t> '+message);
}

function line(){
    console.log('---------------------------------------------------');
}

function timestamp(){
    return new Date().getTime();
}

function prepare_buffer(testcase){
    var buffer = arguments[0];
    if (typeof(testcase)=='string') {
        msg('Test file: '+testcase);
        buffer = fs.readFileSync(testcase);
    }
    else msg('Test buffer: '+testcase);
    return buffer;
}


function test_common(testcase){
    line();
    var buffer = prepare_buffer(testcase);
    msg('Size: '+buffer.length+' bytes');
    msg('Entropy(log2): '+entropy(buffer.toString(), 2));
    msg('Encoding...');
    var timestamp1 = timestamp();
    var encoded = huffman.encode(buffer);
    var crate = (encoded.length/buffer.length)*100;
    var timestamp2 = timestamp();
    msg('Elapsed (~ms): '+(timestamp2-timestamp1));
    msg('Compression rate: '+crate+'%');
    if (crate>100) msg('It made it giant!!!!');
    msg('Decoding...');
    var decoded = huffman.decode(encoded);
    msg('Elapsed (~ms): '+(timestamp()-timestamp2));
    if (buffer.equals(decoded)) msg('ok');
    else msg('FAIL');
    line();
    console.log();
}


function test_for_throwing(testcase){
    line();
    var buffer = prepare_buffer(testcase);
    msg('Size: '+buffer.length+' bytes');
    msg('Check 4 throwing');
    try {
        huffman.decode(encoded, {'is_significant': true});
    }
    catch(e){
        msg('ok');
        line();
        console.log();
        return;
    }
    msg('FAIL');
    line();
    console.log();
}


function test_recoverage(testcase, chkstr){
    line();
    var buffer = prepare_buffer(testcase);
    msg('Size: '+buffer.length+' bytes');
    var timestamp1 = timestamp();
    msg('Decoding damaged...');
    var decoded = huffman.decode(buffer, { 'is_significant' : false });
    var timestamp2 = timestamp();
    msg('Elapsed (~ms): '+(timestamp2-timestamp1));
    if (decoded.toString().indexOf(chkstr)==-1) msg('FAIL');
    else msg('ok');
    line();
    console.log();
}

var uniqbytes = [];
for(var i=0;i<256;i++)
    uniqbytes.push(i);


test_common(Buffer('abcde'));
test_common(Buffer('ababababababab'));
test_common(Buffer('aaaaaaaaaaaaaaaaa'));
test_common(Buffer('a'));
test_common(Buffer('abcdefghijklmnooooooooooooooooooooooooooooopqrstuvwxyz'));
test_common(Buffer('cccccccccccccccccccba'));
test_common(Buffer(uniqbytes));

test_for_throwing('damaged1');
test_recoverage('damaged1', 'Htree.from_raw = function(buffer)');

test_for_throwing('damaged2');
test_recoverage('damaged2', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

test_for_throwing('damaged_badmod');
test_for_throwing('empty');


test_common('exotic.msg');
test_common('already_encoded');
test_common('example.bmp');
test_common('Война и мир.txt');


